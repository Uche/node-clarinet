node-clarinet (0.12.4+dfsg-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + node-clarinet: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 21 Nov 2022 12:11:46 +0000

node-clarinet (0.12.4+dfsg-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Update renamed lintian tag names in lintian overrides.
  * Set upstream metadata fields: Bug-Submit.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).
  * Update standards version to 4.5.0, no changes needed.
  * Remove MIA uploader Andrew Kelley <superjoe30@gmail.com>. Closes: #909893

  [ Yadd ]
  * Bump debhelper compatibility level to 13
  * Modernize debian/watch
    * Fix GitHub tags regex
    * Fix filenamemangle
  * Use dh-sequence-nodejs
  * Update standards version to 4.6.0, no changes needed.
  * Drop dependency to nodejs
  * Add myself as uploader
  * Update lintian overrides

 -- Yadd <yadd@debian.org>  Wed, 22 Dec 2021 16:18:07 +0100

node-clarinet (0.12.4+dfsg-1) unstable; urgency=medium

  * Team upload
  * Declare compliance with policy 4.4.1
  * Add "Rules-Requires-Root: no"
  * Reduce exclusions to enable test
  * New upstream version 0.12.4+dfsg
  * Enable upstream test during autopkgtest

 -- Xavier Guimard <yadd@debian.org>  Tue, 26 Nov 2019 07:28:53 +0100

node-clarinet (0.9.1+dfsg-4) unstable; urgency=medium

  * Team upload
  * Bump debhelper compatibility level to 12
  * Declare compliance with policy 4.4.0
  * Add debian/gbp.conf
  * Add upstream/metadata
  * Switch install to pkg-js-tools

 -- Xavier Guimard <yadd@debian.org>  Sat, 03 Aug 2019 15:42:22 +0200

node-clarinet (0.9.1+dfsg-3) unstable; urgency=medium

  * Add build-dependency on nodejs

 -- Graham Inggs <ginggs@debian.org>  Fri, 24 May 2019 15:33:18 +0000

node-clarinet (0.9.1+dfsg-2) unstable; urgency=medium

  * Team upload
  * Switch to autopkgtest-pkg-nodejs (Closes: #894930)
  * Update Vcs-* fields for move to salsa
  * Switch to Section: javascript, Priority: optional
  * Use secure copyright format URI
  * Drop trailing whitespace from debian/control
  * Use dversionmangle instead of uversionmangle in debian/watch
  * Bump Standards-Version to 4.3.0, no further changes

 -- Graham Inggs <ginggs@debian.org>  Fri, 24 May 2019 13:15:46 +0000

node-clarinet (0.9.1+dfsg-1) unstable; urgency=low

  * dfsg repack to avoid problematic samples and tests files.

 -- Andrew Kelley <superjoe30@gmail.com>  Thu, 02 Oct 2014 19:51:14 -0300

node-clarinet (0.9.0-1) unstable; urgency=low

  * Initial release (Closes: #753652)

 -- Andrew Kelley <superjoe30@gmail.com>  Fri, 12 Sep 2014 10:06:21 -0300
